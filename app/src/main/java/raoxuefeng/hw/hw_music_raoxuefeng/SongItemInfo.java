package raoxuefeng.hw.hw_music_raoxuefeng;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rxf on 16/3/24.
 */
public class SongItemInfo {
    String mSongName = "";
    boolean isPlaying = false;
    boolean isCurrent = false;
    String mURI = "";

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setIsCurrent(boolean isCurrent) {
        this.isCurrent = isCurrent;
        setPlaying(false);
    }



    public String getURI() {
        return mURI;
    }

    public void setURI(String uri) {
        this.mURI = uri;
    }

    public SongItemInfo(String mSongName,String uri) {

        this.mSongName = mSongName;
        this.mURI = uri;
    }

    public String getSongName() {
        return mSongName;
    }

    public void setSongName(String SongName) {
        this.mSongName = SongName;
    }

    public boolean IsPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean isPlaying) {
        this.isPlaying = isPlaying;
        setIsCurrent(isPlaying);
    }

    public static List<SongItemInfo> getSongItemInfoFromMusicInfo(List<MusicInfo> mInfo)
    {
        List<SongItemInfo> sInfo = new ArrayList<>();
        SongItemInfo s;
        for (MusicInfo m :mInfo
             ) {
            s = new SongItemInfo(m.getTitle(),m.getUrl());
            sInfo.add(s);
        }
        return sInfo;
    }
}
