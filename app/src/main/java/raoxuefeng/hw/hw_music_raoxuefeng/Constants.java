package raoxuefeng.hw.hw_music_raoxuefeng;

/**
 * Created by rxf on 16/3/24.
 */
public interface Constants {
    /* service 和 activity的交互 */
    int CMD_UNREGISTER_CLIENT = 1;
    int CMD_REGISTER_CLIENT = 2;
//    int CMD_UPDATE_MUSIC_LIST =  3;
    int CMD_PLAY_THE_SONG = 5;//start from the beginning
    int CMD_PAUSE_SONG = 6;
    int CMD_PLAY_NEXT = 7;
    int CMD_PLAY_PREV = 8;
    int CMD_QUERY_PLAY_STATUS = 9;
    int NOTIFY_UPDATE_PROGRESS = 10;
    int NOTIFY_UPDATE_PLAY_STATUS = 11;
    int CMD_CONTINUE_PLAY = 12;// start from where it is.
    int CMD_SEEK_TO = 13;

    /* service 和 notification的交互 */

}
