package raoxuefeng.hw.hw_music_raoxuefeng;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

/**
 * Created by rxf on 16/3/24.
 */
public class SongListViewAdapter extends BaseAdapter{

    private Context mContext;
    private List<SongItemInfo> mSongListItemInfos;
    private LayoutInflater mLayoutInflater;

    public SongListViewAdapter(Context context, List<SongItemInfo> SongItemInfos) {
        mContext = context;
        mSongListItemInfos = SongItemInfos;
        // 三种获取mLayoutInflater的方式
        mLayoutInflater = ((Activity) mContext).getLayoutInflater();
        mLayoutInflater = LayoutInflater.from(mContext);
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mSongListItemInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return mSongListItemInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return super.getItemViewType(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_music, null);
            viewHolder = new ViewHolder();

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.mTextViewSongName = (TextView) convertView.findViewById(R.id.text_view_song_name);
        viewHolder.mTextViewSongName.setText(mSongListItemInfos.get(position).getSongName());
        SongItemInfo info = mSongListItemInfos.get(position);
        if(info.IsPlaying()){
            // is currently playing.
            viewHolder.mTextViewSongName.setTextColor(Color.rgb(255, 0, 100));
        }else if(info.isCurrent()){
            //is current select.
            viewHolder.mTextViewSongName.setTextColor(Color.rgb(0,0,255));
        }else{
            //not selected
            viewHolder.mTextViewSongName.setTextColor(Color.rgb(0,0,0));
        }
        return convertView;
    }

    class ViewHolder {
        TextView mTextViewSongName;
    }

    public void refreshData(List<SongItemInfo> SongItemInfos) {
        mSongListItemInfos = SongItemInfos;
        this.notifyDataSetChanged();
    }
}
