package raoxuefeng.hw.hw_music_raoxuefeng;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Constants,View.OnClickListener{
    private static final String TAG = MainActivity.class.getSimpleName();

    private Boolean mIsBound = false;
    private IncomingHandler msgHandler = new IncomingHandler();
    private Messenger mMessenger = new Messenger(msgHandler);
    private Messenger mServiceMessenger = null;


    private ImageButton mBtnPlayAndPause;
    private ImageButton mBtnPlayPrevious;
    private ImageButton mBtnPlayNext;
    private SeekBar mSeekBar;
    private ListView mSongList;


    private List<SongItemInfo> mSongItemInfos = new ArrayList<>();
    private List<MusicInfo> mMusicInfos = new ArrayList<>();
    private int mCurSongInListPos = 0;
    private int mCurSongPlayPosition = 0;
    private long mCurSongDuration =  0;
    private MusicLoader mMusicLoader;
    private SongListViewAdapter mSongListAdapter;

    public int getCurSongInListPos()
    {
        return mCurSongInListPos;
    }

    private void initData()
    {
        //get song list.
        mMusicLoader = MusicLoader.instance(this.getContentResolver());
        mMusicInfos = mMusicLoader.getMusicList();
        mSongItemInfos = SongItemInfo.getSongItemInfoFromMusicInfo(mMusicInfos);
//        mSongItemInfos.add(new SongItemInfo("test","test"));
        if (mSongItemInfos.size()>0)
        {
            mSongItemInfos.get(mCurSongInListPos).setIsCurrent(true);
        }
    }
    private void initView()
    {
        mBtnPlayAndPause = (ImageButton) findViewById(R.id.btn_play_pause);
        mBtnPlayNext = (ImageButton) findViewById(R.id.btn_play_next);
        mBtnPlayPrevious = (ImageButton)findViewById(R.id.btn_play_prev);
        mSeekBar = (SeekBar)findViewById(R.id.seek_bar_song);
        mSongList = (ListView) findViewById(R.id.listView);
        mSongListAdapter = new SongListViewAdapter(this, mSongItemInfos);
        mSongList.setAdapter(mSongListAdapter);
        mSongList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, "onItemClick position: " + position + " id: " + id);
                //Change PlaySong.
                mSongItemInfos.get(mCurSongPlayPosition).setIsCurrent(false);
                mCurSongInListPos = position;
                mSongItemInfos.get(mCurSongPlayPosition).setIsCurrent(true);
                mSongListAdapter.notifyDataSetChanged();
                Message msg = Message.obtain(null, CMD_PLAY_THE_SONG);
                msg.arg1 = mCurSongInListPos;
                sendMsgToService(msg);

            }
        });
        mBtnPlayAndPause.setOnClickListener(this);
        mBtnPlayPrevious.setOnClickListener(this);
        mBtnPlayNext.setOnClickListener(this);

        mSeekBar.setOnClickListener(this);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser)
                {
                    PlaySongSeekTo(progress);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void PlaySongSeekTo(int progress) {
        Message msg = Message.obtain(null, CMD_SEEK_TO);
        msg.arg1 = progress;
        sendMsgToService(msg);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();//get song list from contentResolver.
        initView();

        //Bind Music Service.Start it if not started yet.
        doBindService();
    }

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "service disconnected.");
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.e(TAG, "service connected.");
            if (mServiceMessenger == null) {
                mServiceMessenger = new Messenger(service);
                Log.e(TAG, "mServiceMessenger new Messenger" + mServiceMessenger);
            }
            if (mServiceMessenger != null) {
                Log.e(TAG, "@onServiceConnected, mServiceMessenger not null.");
                try {
                    Message msg = Message.obtain(null, CMD_REGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mServiceMessenger.send(msg);
                    mIsBound = true;

                } catch (Exception e) {
                    Log.i(TAG, "send register message failed.");
                }
            } else {
                Log.e(TAG, "onServiceConnected :mServiceMessenger is null");
            }
        }
    };
    private void startOwnService() {
        if (!ServiceHelper.isServiceRunning(this, MusicService.class.getName())) {
            Log.i(TAG, "Screen Server is not start, start it now.");
            Intent intent = new Intent();
            intent.setClass(MainActivity.this, MusicService.class);
            ComponentName cn = MainActivity.this.startService(intent);
            if (cn == null) {
                Log.i(TAG, "service start failed.");
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startOwnService();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startOwnService();
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        boolean bResult = this.getApplicationContext().bindService(new Intent(this, MusicService.class),
                mConnection, Context.BIND_AUTO_CREATE);
        if (bResult) {
            Log.i(TAG, "call bindService correct.");
        } else {
            Log.i(TAG, "call bindService failed.");
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        startOwnService();
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mServiceMessenger != null) {
                try {
                    Message msg = Message.obtain(null,
                            CMD_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mServiceMessenger.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
                // Detach our existing connection.
                this.getApplication().unbindService(mConnection);
            }
            mIsBound = false;
        }
    }
    void sendMsgToService(Message msg)
    {
        try {
            msg.replyTo = mMessenger;
            mServiceMessenger.send(msg);
        } catch (Exception e) {
            Log.i(TAG, "send register message failed.");
        }
    }

    @Override
    protected void onPause() {
        doUnbindService();
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_play_next:
                sendMsgToService(Message.obtain(null,CMD_PLAY_NEXT));
                break;
            case R.id.btn_play_prev:
                sendMsgToService(Message.obtain(null,CMD_PLAY_PREV));
                break;
            case R.id.btn_play_pause:
                if(mSongItemInfos.get(mCurSongInListPos).IsPlaying()) {
                    sendMsgToService(Message.obtain(null, CMD_PAUSE_SONG));
                }
                else{
                    if(mSeekBar.getProgress()==0)
                    {
                        Message msg = Message.obtain(null, CMD_PLAY_THE_SONG);
                        msg.arg1 = mCurSongInListPos;
                        sendMsgToService(msg);
                        Log.e(TAG, "Play the Song !!! mCurSongInListPos:" + mCurSongInListPos);
                    }else
                    {
                        sendMsgToService(Message.obtain(null,CMD_CONTINUE_PLAY));
                    }

                }
            break;
            case R.id.seek_bar_song:break;
            default:break;
        }
    }

    //本程序的handler
    private class IncomingHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case NOTIFY_UPDATE_PLAY_STATUS:
                    int SongId = msg.arg1;
                    int isPlaying = msg.arg2;
                    mCurSongInListPos = SongId;
                    Log.e(TAG, "!!! NOTIFY_UPDATE_PLAY_STATUS SongId: " + SongId + "isPlaying: " + isPlaying);
                    mSongItemInfos.get(mCurSongInListPos).setIsCurrent(true);
                    mSongItemInfos.get(mCurSongInListPos).setPlaying((isPlaying!=0));
                    break;
                case NOTIFY_UPDATE_PROGRESS:
                    int progress = msg.arg1;
                    Log.e(TAG, "receive updated progress: " + progress);
                    mSeekBar.setProgress(progress);
                    break;

//                case NTF_UPDATE_SELECTED_LIGHTINFO:
//                {
//                    if(mJustOperated){
//                        mJustOperated = false;
//                        Log.e(TAG, "刚操作完第一次刷新值跳过更新 mJustOperated !!!");
//                        return;
//                    }
//                    Bundle bundle = msg.getData();
//                    String light_macName = bundle.getString("macname");
//                    String light_aName = bundle.getString("aname");
//                    boolean isOnline = bundle.getBoolean("online");
//                    Log.e(TAG, "NTF_UPDATE_CURRENT_LIGHT_INFO light_aName = " + light_aName + " online:" + isOnline + " lux: " + bundle.getInt("lvl") + " OnOff: " + bundle.getBoolean("onoff"));
//
//                    //灯的mac地址和名字不为空，灯在线，才接受LED Service同步上来的亮度值
//                    if( (light_macName!=null) && (!light_macName.equals("")) && (isOnline))
//                    {
//                        isHasCurrentLight = true;
//                        //更新当前亮度
//                        ////////////////////////////////////
//                        current = bundle.getInt("lvl");
//                        ////////////////////////////////////
//
//                        boolean on_off = bundle.getBoolean("onoff");
//                        if (on_off) {
//                            //设置成On的图片
//                            try {
//                                if((current * 0.001) < 0.2)
//                                {   //避免alpha太小，就黑乎乎什么都看不见了。
//                                    AdjustActivity.backPictureView.setAlpha((float) (0.2));
//                                }else{
//                                    AdjustActivity.backPictureView.setAlpha((float) (current*0.001));
//                                }
//                                backPictureView.setImageBitmap(
//                                        decodeSampledBitmapFromResource(resources, R.drawable.light_on, 200, 200)); //FIXME:根据屏幕宽高比例来计算
//                            } catch (OutOfMemoryError err) {
//                                Log.e(TAG, "加载开灯图片资源失败");
//                            }
//
//
//                            // alpha值范围在0-1，current值范围在0-1000，
//                            // alpha如果算出来<0.2就不更新到界面上了，因为太暗看不出来。
//                            if ((current * 0.001) > 0.2)
//                            {
//
//                                //如果正在moving，手动调节为主，sync的值就不用，因为会闪屏
//                                if(!isInMoving)
//                                {
//                                    AdjustActivity.backPictureView.setAlpha((float) (current * 0.001));
//                                    Log.d(TAG, "NTF_UPDATE_CURRENT_LIGHT_INFO 更新当前亮度");
//                                }else
//                                {
//                                    Log.d(TAG, "NTF_UPDATE_CURRENT_LIGHT_INFO 跳过 更新当前亮度");
//                                }
//
//                            }else
//                            {
//                                Log.d(TAG, "NTF_UPDATE_CURRENT_LIGHT_INFO current * 0.001 <= 0.3");
//                            }
//                        }
//                        else
//                        {
//                            //设置成Off的图片
//                            try {
//                                backPictureView.setImageBitmap(
//                                        decodeSampledBitmapFromResource(resources, R.drawable.light_off, 200, 200)); //FIXME:根据屏幕宽高比例来计算
//                            } catch (OutOfMemoryError err) {
//                                Log.e(TAG, "加载关灯图片资源失败");
//                            }
//
//                        }
//                    }
//                    else
//                    {
//                        //没有灯
//                        isHasCurrentLight = false;
//                        Log.e(TAG, "NTF_UPDATE_CURRENT_LIGHT_INFO 获取到当前灯的mac信息为空，没有选中灯");
//                    }
//                }
//                break;
                default:
                    break;
            }
        }
    }
}
