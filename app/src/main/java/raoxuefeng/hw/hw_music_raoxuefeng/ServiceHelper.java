package raoxuefeng.hw.hw_music_raoxuefeng;

import android.app.ActivityManager;
import android.content.Context;

import java.util.List;

/**
 * Created by like on 15-8-11.
 *
 */
public class ServiceHelper {
    private static int MaxListNum = 500;

    public static boolean isServiceRunning(Context mContext, String className) {
        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager)
                mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList
                = activityManager.getRunningServices(MaxListNum);
        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className)) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    public static void setMaxServiceSearchNum(int num) {
        MaxListNum = num;
    }
}
